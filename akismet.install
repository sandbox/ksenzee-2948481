<?php

/**
 * @file
 * Install and uninstall functions as well as schema definition for the Akismet module.
 */

/**
 * @file
 * Install and uninstall functions as well as schema definition for the Akismet module.
 */

use Drupal\Core\Url;
use Drupal\akismet\Utility\AkismetUtilities;
use Akismet\Client\Client;

/**
 * Implements hook_requirements().
 *
 * @param $check
 *   (optional) Boolean whether to re-check the module's installation and
 *   configuration status. Defaults to TRUE, as this argument is not passed for
 *   hook_requirements() by default. Passing FALSE allows other run-time code
 *   to re-generate requirements error messages to be displayed on other pages
 *   than the site's system status report page.
 *
 * @return array
 *
 * @see \Drupal\akismet\EventSubscriber\Subscriber::onRequest()
 * @see Drupal\Akismet\Form\Settings::buildForm()
 * @see AkismetUtilities::getAPIKeyStatus()
 */
function akismet_requirements($phase = 'runtime', $check = TRUE) {
  $requirements = array();
  if ($phase == 'runtime') {
    $status = AkismetUtilities::getAPIKeyStatus($check);

    $requirements['akismet'] = array(
      'title' => t('Akismet API key'),
      'value' => t('Valid'),
    );
    // Immediately return if everything is in order.
    if ($status['isVerified']) {
      return $requirements;
    }
    // If not, something is wrong; prepare the requirements entry and set
    // defaults for any yet unknown edge-cases.
    $requirements['akismet']['severity'] = REQUIREMENT_ERROR;
    // Append a link to the settings page to the error message on all pages,
    // except on the settings page itself. These error messages also need to be
    // shown on the settings page, since Akismet API keys can be entered later.
    $admin_message = '';
    /** @var $current_route \Drupal\Core\Routing\CurrentRouteMatch */
    $current_route = \Drupal::service('current_route_match');
    if ($current_route->getRouteName() !== 'akismet.settings') {
      $admin_message = t('Visit the <a href="@settings-url">Akismet settings page</a> to add your API key.', array(
        '@settings-url' => Url::fromRoute('akismet.settings')->toString(),
      ));
    }
    // Generate an appropriate error message:
    $akismet = \Drupal::service('akismet.client');
    // Missing API keys.
    if (!$status['isConfigured']) {
      $requirements['akismet']['value'] = t('Not configured');
      $requirements['akismet']['description'] = t('The Akismet API key is not configured yet. @admin-message', array(
        '@admin-message' => $admin_message,
      ));
    }
    // Invalid API keys.
    elseif ($status['response'] === $akismet::AUTH_ERROR) {
      $requirements['akismet']['value'] = t('Invalid');
      $requirements['akismet']['description'] = t('The configured Akismet API key is invalid. @admin-message', array(
        '@admin-message' => $admin_message,
      ));
    }
    // Communication error.
    elseif ($status['response'] === $akismet::NETWORK_ERROR) {
      $requirements['akismet']['value'] = t('Network error');
      $requirements['akismet']['description'] = t('The Akismet server could not be contacted. Please make sure that your web server can make outgoing HTTP requests.');
    }
    // 404, etc.
    else {
      $requirements['akismet']['value'] = t('Unknown error');
      $requirements['akismet']['description'] = t('The API key could not be verified.');
    }
  }
  return $requirements;
}


/**
 * Implements hook_schema().
 */
function akismet_schema() {
  $schema['akismet'] = array(
    'description' => 'Stores Akismet responses for content.',
    'fields' => array(
      'entity' => array(
        'description' => 'Entity type of the content.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'id' => array(
        'description' => 'Unique entity ID of the content.',
        'type' => 'varchar',
        'length' => 36,
        'not null' => TRUE,
        'default' => '',
      ),
      'form_id' => array(
        'description' => 'The form_id of the form being protected.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'guid' => array(
        'description' => 'The GUID returned by Akismet.',
        'type' => 'varchar',
        'length' => 36,
        'not null' => TRUE,
        'default' => '',
      ),
      'changed' => array(
        'description' => 'Unix timestamp when the data was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'moderate' => array(
        'description' => 'Whether the content needs to be moderated.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'classification' => array(
        'description' => 'Result of text analysis: "ham", "spam" or "unsure".',
        'type' => 'varchar',
        'length' => 16,
        'not null' => FALSE,
      ),
      'request' => array(
        'description' => 'The original data sent to Akismet for analysis.',
        'type' => 'blob',
        'serialize' => TRUE,
      ),
    ),
    'indexes' => array(
      'guid' => array('guid'),
    ),
    'primary key' => array('entity', 'id'),
    'foreign keys' => array(
      'akismet_form_id' => array(
        'table' => 'akismet_form',
        'columns' => array(
          'form_id' => 'form_id',
        ),
      ),
    ),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function akismet_install() {
  $messenger = \Drupal::messenger();
  $config_url = Url::fromUri('base://admin/config/content/akismet/settings');
  $messenger->addMessage(t('Akismet installed successfully. Visit the @link to add your API key.', [
    '@link' => \Drupal::l(t('Akismet settings page'), $config_url),
  ]));
}

/**
 * Implements hook_uninstall().
 */
function akismet_uninstall() {
  //Drupal::cache()->deleteTags(array('akismet'));
  $state_keys = ['akismet.testing_use_local', 'akismet.testing_create_keys', 'akismet.omit_warnings'];
  \Drupal::state()->deleteMultiple($state_keys);
}
