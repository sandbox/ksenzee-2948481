<?php

namespace Drupal\akismet\Client\Exception;

/**
 * Akismet authentication error exception.
 *
 * Thrown in case API keys or other authentication parameters are invalid.
 */
class AkismetAuthenticationException extends AkismetException {
}
